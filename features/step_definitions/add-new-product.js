const assert = require('assert');
const { Given, When, Then } = require('cucumber');
const { Builder, By, Capabilities, Key, until, JavascriptExecutor } = require('selenium-webdriver');
const { expect } = require('chai');

require("chromedriver");

// driver setup
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });
const driver = new Builder().withCapabilities(capabilities).build();

Given('Estoy en la pestaña {string}', async function (tabName) {
    await driver.get('http://localhost:8080/');
    let tabButton = await driver.wait(until.elementLocated(By.id("tab-button-" + tabName.replace(" ",""))));
    tabButton.click();
  });

  When('Abro sobre el menú flotante', async function () {
    pulsarOpcionMenuFlotante("menu")
  });

  When('Pulso en la opción + del menú', async function () {
    pulsarOpcionMenuFlotante("addProduct");
  });

  When('Escribo {string} sobre la caja de text', async function (productName) {
    let idInputText = "product2Add";
    let inputText = await driver.wait(until.elementLocated(By.css("#product2Add")));
    let script = "document.getElementById('product2Add').setAttribute('value', '" + productName + "');"
    console.log(script);
    (await driver).executeScript(script);
    
    
    //(await driver).executeScript("arguments[0].value = '" + productName + "';", inputText);
    //inputText.click();
    //inputText.sendKeys(productName);
  });

  When('Pulso sobre el botón Añadir', async function (labelButton) {
    let button = (await driver).findElement(By.id("anadir"));
    //(await driver).executeScript("arguments[0].click();", button);
    //button.click();
  });

  Then('The product {string} is in {string} Tab', async function (string, string2) {
    // Write code here that turns the phrase above into concrete actions
    (await driver).close();
    return 'pending';
  });

  async function pulsarOpcionMenuFlotante(idOpcion){
    let button = (await driver).findElement(By.id(idOpcion));
    
    (await driver).executeScript("arguments[0].click();", button);
  }

  