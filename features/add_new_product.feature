Feature: Add new product
    In order to add new product on Home Tab
    As a user
    To want feature to add new products to tabs

Scenario: Quiero agregar un nuevo producto
    Given Estoy en la pestaña "En Despensa"
    When Abro sobre el menú flotante
        And Pulso en la opción + del menú
        And Escribo "zanahoria" sobre la caja de text
        And Pulso sobre el botón Añadir
    Then el producto "Zanahoria" debe aparecer en la pestaña "En Despensa"



    