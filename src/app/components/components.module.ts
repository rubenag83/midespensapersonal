import { ProductsComponent } from './productos/products.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from './menu/menu.component';
import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';

@NgModule({
    declarations: [ProductsComponent, MenuComponent, MenuLateralComponent],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
      ],
    exports:[ProductsComponent, MenuComponent, MenuLateralComponent]
})

export class ComponentsModule{}