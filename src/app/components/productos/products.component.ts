import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Product } from 'src/app/classes/product';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {

  @Input() public list2Control : Array<Product>;
  @Input() public listTitle : String;
  @Input() public query: String;
  @Input() public showToHome : Boolean = true;
  @Input() public showToUp : Boolean = true;
  @Input() public showToProvisions : Boolean = true;
  @Input() public showToBuyProduct : Boolean = true;  

  @Output() actionElementEvent: EventEmitter<number> = new EventEmitter<number>();
  @Output() actionDeleteItem: EventEmitter<number> = new EventEmitter<number>();
  @Output() actionToHome: EventEmitter<number> = new EventEmitter<number>();
  @Output() actionToUp: EventEmitter<number> = new EventEmitter<number>();
  @Output() actionToProvisions: EventEmitter<number> = new EventEmitter<number>();
  @Output() actionToBuy: EventEmitter<number> = new EventEmitter<number>();

  constructor() { 
  }

  ngOnInit() {
    this.eventSearchON();
  }

  protected eventSearchON(){
    const searchbar = document.querySelector('ion-searchbar#productsSearch');
    searchbar.addEventListener('ionInput', this.searchHandleInput);
  }

  searchHandleInput(event) {
    const query = event.target.value.toLowerCase();
    const htmlItems = Array.from(document.querySelector('ion-list#elementsList').children);
    console.log(htmlItems);
    requestAnimationFrame(() => {
      htmlItems.forEach(htmlItem => {
        const shouldShow = htmlItem.textContent.toLowerCase().indexOf(query) > -1;
        
        (htmlItem as HTMLElement).style.display = shouldShow ? 'block' : 'none';
      });
    });
  }

  public clickElement(index: number){
    this.actionElementEvent.emit(index);
  }

  public deleteItem(index: number){
    this.actionDeleteItem.emit(index);
  }

  public toHome(index: number){
    this.actionToHome.emit(index);
  }

  public toUp(index: number){
    this.actionToUp.emit(index);
  }

  public toProvisions(index: number){
    this.actionToProvisions.emit(index);
  }

  public toBuy(index: number){
    this.actionToBuy.emit(index);
  }
}
