import { AddProductModalPage } from './../../pages/add-product-modal/add-product-modal.page';
import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { isNull, isNullOrUndefined } from 'util';
import { Product } from 'src/app/classes/product';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  @Input() public listName: string;

  @Output() actionChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() addElementEvent: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() copyList2Clipboard: EventEmitter<null> = new EventEmitter();

  private action: string;
  public actionsTitle = new Array();
  public actionTitle: String;

  public static NO_ACTION: string = "NONE"; 
  public static ACTION_ADD_1: string = "ADD_1";
  public static ACTION_MINUS_1: string = "MINUS_1"; 
  
  private static NO_ACTION_TITLE = "";
  private static ACTION_ADD1_TITLE = "+1";
  private static ACTION_MINUS1_TITLE = "-1";

  constructor(@Inject(AlertController) private alertController, @Inject(ModalController) public modalController) { 

    this.action = MenuComponent.NO_ACTION;

    this.actionsTitle[MenuComponent.NO_ACTION] = MenuComponent.NO_ACTION_TITLE;
    this.actionsTitle[MenuComponent.ACTION_ADD_1] = MenuComponent.ACTION_ADD1_TITLE;
    this.actionsTitle[MenuComponent.ACTION_MINUS_1] = MenuComponent.ACTION_MINUS1_TITLE;
  }

  ngOnInit() {}

  async onAddBtnClicked(){
    const modal = await this.modalController.create({
      component: AddProductModalPage
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data != undefined && !data.dismissed)
      this.addElementEvent.emit(data.name);
  }

  changeAction(action: string){
    this.action = action;
    this.actionTitle = this.actionsTitle[this.action];
    this.actionChanged.emit(this.action);
  }

  public static actionElement(product: Product, action: string): Product{
    if (!isNull(product)){
      switch(action){
        case MenuComponent.ACTION_ADD_1:
          product.count++;
          break;
        case MenuComponent.ACTION_MINUS_1:
          if (product.count > 0){
            product.count--;
          }
          break;
      }
    }    
    return product;    
  }

  onCopyList2Clipboard(){
    this.copyList2Clipboard.emit();
  }


  public staticNoAction(){
    return MenuComponent.NO_ACTION;
  }

  public staticActionAdd1(){
    return MenuComponent.ACTION_ADD_1;
  }

  public staticActionMinus1(){
    return MenuComponent.ACTION_MINUS_1;
  }

}
