import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirDespensaPage } from './subir-despensa.page';

describe('SubirDespensaPage', () => {
  let component: SubirDespensaPage;
  let fixture: ComponentFixture<SubirDespensaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubirDespensaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubirDespensaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
