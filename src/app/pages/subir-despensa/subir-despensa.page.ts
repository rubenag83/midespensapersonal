import { Component, OnInit } from '@angular/core';
import { ProductsArraysManagement } from '../../classes/products-arrays-management';
import { ListPage } from '../list-page';

@Component({
  selector: 'app-subir-despensa',
  templateUrl: './subir-despensa.page.html',
  styleUrls: ['./subir-despensa.page.scss'],
})
export class SubirDespensaPage  extends ListPage implements OnInit {

  ngOnInit() {
    this.listName = ProductsArraysManagement.LISTA_SUBIR_DESPENSA;
  }

  ionViewWillEnter (){
    this.loadList();
  }

}
