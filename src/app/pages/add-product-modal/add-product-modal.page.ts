import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ProductsArraysManagement } from 'src/app/classes/products-arrays-management';
import { Product } from 'src/app/classes/product';
import { ModalController } from '@ionic/angular';
import { ProductArrayProcessing } from 'src/app/classes/product-array-processing';

@Component({
  selector: 'app-add-product-modal',
  templateUrl: './add-product-modal.page.html',
  styleUrls: ['./add-product-modal.page.scss'],
})
export class AddProductModalPage implements OnInit {

  @Output() productSelected: EventEmitter<string> = new EventEmitter<string>();

  private elementsManagement: ProductsArraysManagement;
  public listProduct : Array<Product>;
  public productName : String;

  constructor(@Inject(Storage) private storage, @Inject(ModalController) public modalController, @Inject(ProductArrayProcessing) private productArrayProcessing) { 
  }

  ngOnInit() {

    this.elementsManagement = new ProductsArraysManagement(this.storage, this.productArrayProcessing);
    this.elementsManagement.getList(ProductsArraysManagement.LISTA_PRODUCTOS).then((lista) => {
      this.listProduct = lista;
    });

    const searchbar = document.querySelector('ion-input#product2Add');
    searchbar.addEventListener('ionInput', this.searchHandleInput);
  }

  searchHandleInput(event) {
    const query = event.target.value.toLowerCase();
    const htmlItems = Array.from(document.querySelector('ion-list#newProductList').children);
    console.log(htmlItems);
    requestAnimationFrame(() => {
      htmlItems.forEach(htmlItem => {
        console.log("HTMLItem text: " + htmlItem.textContent);
        const shouldShow = htmlItem.textContent.toLowerCase().indexOf(query) > -1;
        console.log("Shold Show:" + shouldShow);
        (htmlItem as HTMLElement).style.display = shouldShow ? 'block' : 'none';
      });
    });
  }

  clickElement(index: number){
    this.productName = this.listProduct[index].name;
  }

  dismissModal(event) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  addProduct(){
    this.modalController.dismiss({
      'name': this.productName,
      'dismissed': false
    });
  }

}
