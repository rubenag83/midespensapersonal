import { Component, OnInit } from '@angular/core';
import { ProductsArraysManagement } from 'src/app/classes/products-arrays-management';
import { ListPage } from '../list-page';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.page.html',
  styleUrls: ['./comprar.page.scss'],
})
export class ComprarPage extends ListPage implements OnInit {

  ngOnInit() {
    this.listName = ProductsArraysManagement.LISTA_COMPRA;
  }

  ionViewWillEnter (){
    this.loadList();
  }
}
