import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StorageMock } from 'ionic-mocks';
import { ToastController } from '@ionic/angular';

import { EnCasaPage } from './en-casa.page';
import { ProductsArraysManagement } from 'src/app/classes/products-arrays-management';
import { Product } from 'src/app/classes/product';

describe('EnCasaPage', () => {
  let component: EnCasaPage;
  let fixture: ComponentFixture<EnCasaPage>;
  let storageSpy, clipboardSpy, menuControllerSpy;
  let productArray : Array<Product> = [];

  beforeEach(async(() => {

    storageSpy = jasmine.createSpyObj('Storage', ['get', 'set', 'clear', 'remove', 'ready']);
    clipboardSpy = jasmine.createSpyObj('Clipboard', ['copy', 'paste', 'clean']);
    menuControllerSpy = jasmine.createSpyObj('MenuController', ['open', 'close', 'toggle', 'enable', 'swipeEnable','swipeGesture','isOpen', 'isEnabled', 'get', 'getOpen', 'getMenus']);

    TestBed.configureTestingModule({
      declarations: [ EnCasaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnCasaPage);
    fixture.componentInstance = new EnCasaPage(storageSpy, new ToastController, clipboardSpy, menuControllerSpy);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
