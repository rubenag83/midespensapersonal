import { ListPage } from './../list-page';
import { Component, OnInit } from '@angular/core';
import { ProductsArraysManagement } from 'src/app/classes/products-arrays-management';

@Component({
  selector: 'app-en-casa',
  templateUrl: './en-casa.page.html',
  styleUrls: ['./en-casa.page.scss'],
})

export class EnCasaPage extends ListPage implements OnInit {

  ngOnInit() {
    this.listName = ProductsArraysManagement.LISTA_EN_CASA;
  }

  ionViewWillEnter (){
    this.loadList();
  }

  
}
