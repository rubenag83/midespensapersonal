import { ListPage } from './list-page';
import { async } from '@angular/core/testing';
import { ToastController } from '@ionic/angular';

describe('ListPage', () => {
  let storageSpy, clipboardSpy, menuControllerSpy, productArrayProcessing;

  beforeEach(async(() => {
    storageSpy = jasmine.createSpyObj('Storage', ['get', 'set', 'clear', 'remove', 'ready']);
    clipboardSpy = jasmine.createSpyObj('Clipboard', ['copy', 'paste', 'clean']);
    menuControllerSpy = jasmine.createSpyObj('MenuController', ['open', 'close', 'toggle', 'enable', 'swipeEnable','swipeGesture','isOpen', 'isEnabled', 'get', 'getOpen', 'getMenus']);
    productArrayProcessing = jasmine.createSpyObj('ProductArrayProcessing', ['cleanList', 'arraySort']);

  }));

  it('should create an instance', () => {
    expect(new ListPage(storageSpy, new ToastController, clipboardSpy, menuControllerSpy)).toBeTruthy();
  });
});
