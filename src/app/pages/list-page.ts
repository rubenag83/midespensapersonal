import { ProductsArraysManagement } from '../classes/products-arrays-management';
import { Storage } from '@ionic/storage';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { ToastController, MenuController } from '@ionic/angular';
import { Product } from 'src/app/classes/product';
import { MenuComponent } from '../components/menu/menu.component';
import { ProductArrayProcessing } from '../classes/product-array-processing';
import { TextCorrections } from '../classes/text/text-corrections';
import { Inject } from '@angular/core';

export class ListPage {

  public listName: string;
  public action: string;
  public query: string;

  private elementsManagement: ProductsArraysManagement;
  public list2Control : Array<Product>;
  private productArrayProcessing: ProductArrayProcessing

  constructor(@Inject(Storage) private storage, @Inject(ToastController) public toastController, @Inject(Clipboard) private clipboard, @Inject(MenuController) private menu){

  }

  protected loadList(){
    this.productArrayProcessing = new ProductArrayProcessing(new TextCorrections);
    this.elementsManagement = new ProductsArraysManagement(this.storage, this.productArrayProcessing);
    console.log("Componente: " + this.listName);
    this.elementsManagement.getList(this.listName).then((lista) => {
      this.list2Control = lista;
      console.log("Información a mostrar: " + JSON.stringify(this.list2Control));
    });
  }

  actionElement (index:number){
    if (index >= 0 && index < this.list2Control.length){
      MenuComponent.actionElement(this.list2Control[index], this.action)
      
      if (this.list2Control[index].count == 0){
        this.deleteItem(index);
      }
      else{
        this.elementsManagement.setList(this.listName, this.list2Control);
      }
    }
  }

  deleteItem (index:number){
    if (index >= 0 && index < this.list2Control.length){
      this.list2Control.splice(index,1);
    }
    this.elementsManagement.setList(this.listName, this.list2Control);
  }

  toHome(index:number){
    this.moveProductTo(index, ProductsArraysManagement.LISTA_EN_CASA);
  }

  toUp(index:number){
    this.moveProductTo(index, ProductsArraysManagement.LISTA_SUBIR_DESPENSA);
  }

  toProvisions(index:number){
    this.moveProductTo(index, ProductsArraysManagement.LISTA_EN_DESPENSA);
  }

  toBuy(index:number){
    this.moveProductTo(index, ProductsArraysManagement.LISTA_COMPRA);
  }

  moveProductTo (index:number, newListName: string){
    if (index >= 0 && index < this.list2Control.length){
      const product = this.list2Control[index];
      this.elementsManagement.addElementToList(newListName, product.name.toString() ,product.count);
      this.deleteItem(index);
    }
  }

  addElement(productName: string){
    this.list2Control = this.elementsManagement.addElementToList(this.listName, productName);
  }

  actionChanged (action:string){
    this.action = action;
  }

  copyList2Clipboard(){
    var listadoText = "";
    this.list2Control.forEach(producto => {
      listadoText += producto.name + "\n"
    });
    this.clipboard.copy(listadoText);
    this.presentToast("La lista ha sido copiada");
  }

  async presentToast(textMessage: string ) {
    const toast = await this.toastController.create({
      message: textMessage,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  tougleSideMenu(menu) {
    this.menu.enable(true, menu);
    this.menu.toggle(menu);
  }
}
