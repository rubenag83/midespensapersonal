import { Component, OnInit } from '@angular/core';
import { ProductsArraysManagement } from 'src/app/classes/products-arrays-management';
import { ListPage } from '../list-page';

@Component({
  selector: 'app-en-despensa',
  templateUrl: './en-despensa.page.html',
  styleUrls: ['./en-despensa.page.scss'],
})
export class EnDespensaPage extends ListPage implements OnInit {

  ngOnInit() {
    this.listName = ProductsArraysManagement.LISTA_EN_DESPENSA;
  }

  ionViewWillEnter (){
    this.loadList();
  }

}
