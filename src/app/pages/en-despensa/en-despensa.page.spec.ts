import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnDespensaPage } from './en-despensa.page';

describe('EnDespensaPage', () => {
  let component: EnDespensaPage;
  let fixture: ComponentFixture<EnDespensaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnDespensaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnDespensaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
