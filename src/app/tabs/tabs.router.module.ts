import { SubirDespensaPage } from '../pages/subir-despensa/subir-despensa.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'EnCasa',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/en-casa/en-casa.module').then(m => m.EnCasaPageModule)
          }
        ]
      },
      {
        path: 'SubirDespensa',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/subir-despensa/subir-despensa.module').then(m => m.SubirDespensaPageModule)
          }
        ]
      },
      {
        path: 'EnDespensa',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/en-despensa/en-despensa.module').then(m => m.EnDespensaPageModule)
          }
        ]
      },
      {
        path: 'Comprar',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pages/comprar/comprar.module').then(m => m.ComprarPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/EnCasa',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/EnCasa',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
