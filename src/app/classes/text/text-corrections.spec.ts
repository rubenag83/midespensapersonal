import { TextCorrections } from './text-corrections';

describe('La corrección de textos', () => {
  it('debe poderse instanciar', () => {
    expect(new TextCorrections()).toBeTruthy();
  });
  it('si recibe un valor null o string vacio debe devolver una cadena vacía', () => {
    var textCorrections = new TextCorrections();
    expect(textCorrections.setCapitalCase(null)).toBe("");
  });
  it('si recibe un texto en minusculas pondrá la primera letra en mayusculas', () => {
    var textCorrections = new TextCorrections();
    expect(textCorrections.setCapitalCase("hola")).toBe("Hola");
  });
  it('si recibe un texto con la primera letra en mayusculas no hará nada', () => {
    var textCorrections = new TextCorrections();
    expect(textCorrections.setCapitalCase("Hola")).toBe("Hola");
  });
});
