import { isNullOrUndefined } from 'util';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
  })
export class TextCorrections {

    public setCapitalCase (text: String): String {
        var convertText: String = "";
        if (! isNullOrUndefined(text)){
            convertText = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
        }
        return convertText;
    }
}
