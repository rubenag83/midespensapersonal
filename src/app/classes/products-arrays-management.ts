import { isNullOrUndefined } from 'util';
import { Storage } from '@ionic/storage';
import { Product } from 'src/app/classes/product';
import { ProductArrayProcessing } from './product-array-processing';
import { Inject } from '@angular/core';

/*
  Se quiere guardar en Dropbox un fichero con las listas. Se ha encontrado
  este tutorial para intentar seguirlo:
  https://www.joshmorony.com/integrating-ionic-2-with-the-dropbox-api-part-1/
  https://www.joshmorony.com/integrating-ionic-2-with-the-dropbox-api-part-2/
*/
export class ProductsArraysManagement {

    public static LISTA_COMPRA = "ListaCompra";
    public static LISTA_EN_CASA = "ListaEnCasa";
    public static LISTA_EN_DESPENSA = "ListaEnDespensa";
    public static LISTA_SUBIR_DESPENSA = "ListaSubirDespensa";
    public static LISTA_PRODUCTOS = "ListaProductos";

    private static listUpdate: Array<boolean> = [];
    private lists: Array<Array<Product>> = [];
    
    constructor(@Inject(Storage) private storage, @Inject(ProductArrayProcessing) private productArrayProcessing){
        /*this.lists[ProductsArraysManagement.LISTA_COMPRA] = [];
        this.lists[ProductsArraysManagement.LISTA_EN_CASA] = [];
        this.lists[ProductsArraysManagement.LISTA_EN_DESPENSA] = [];
        this.lists[ProductsArraysManagement.LISTA_SUBIR_DESPENSA] = [];*/

        if (ProductsArraysManagement.listUpdate.length == 0){
          ProductsArraysManagement.listUpdate[ProductsArraysManagement.LISTA_COMPRA] = false;
          ProductsArraysManagement.listUpdate[ProductsArraysManagement.LISTA_EN_CASA] = false;
          ProductsArraysManagement.listUpdate[ProductsArraysManagement.LISTA_EN_DESPENSA] = false;
          ProductsArraysManagement.listUpdate[ProductsArraysManagement.LISTA_SUBIR_DESPENSA] = false;
        }
        
    }

    public async getList(listName: string): Promise<Array<Product>>{
      if (isNullOrUndefined(this.lists[listName]) || ProductsArraysManagement.listUpdate[listName] == true){
        return new Promise(listRetrieve =>
          this.storage.ready().then(()=>{
              return this.storage.get(listName).then((val) => {
                ProductsArraysManagement.listUpdate[listName] = false;
                if (isNullOrUndefined(val)){
                  val = new Array<Product>();
                }

                val = this.productArrayProcessing.cleanList(val);

                this.lists[listName] = this.productArrayProcessing.arraySort(val);

                listRetrieve(this.lists[listName]);
              }, error => {
                console.error('erreur : ', JSON.stringify(error));
                listRetrieve(new Array<Product>());
              });
            }).catch(err => {
                  console.error('storage not ready');
                  console.error('err : ' + err);
                  listRetrieve(new Array<Product>());
            }));
      }
      else{
        console.log(listName + " la encuentra y tiene: " + this.lists[listName]);
        return this.lists[listName];
      }          
    }

    

    public setList(listName: string, list: Array<Product>){
        this.lists[listName] = list;
        this.saveList(listName, this.lists[listName]);
    }

    private saveList(listName: string, list:Array<Product>){
        this.storage.ready().then(()=>{
          this.storage.set(listName, list).then(_=> {
            ProductsArraysManagement.listUpdate[listName] = true;
          }, error => {
            console.log('erreur : ', JSON.stringify(error))
          });
        }).catch(err => {
              console.log('storage not ready');
              console.log('err : ', JSON.stringify(err));
        });
    }

    public addElementToList(listName: string, productName: string, count: number = 1) : Array<Product>{
      if (isNullOrUndefined(this.lists[listName])){
        this.lists[listName] = new Array<Product>();
      }

      if (!isNullOrUndefined(productName) && count >= 1){
        productName = productName.charAt(0).toUpperCase() + productName.substr(1).toLowerCase();
        var newProduct =new Product ();
        newProduct.name = productName;
        newProduct.count = count;

        this.lists[listName].push(newProduct);  

        this.saveList(listName, this.lists[listName]);
        if (listName != ProductsArraysManagement.LISTA_PRODUCTOS)
          this.addProductIntoProductList(productName);
      }
      return this.lists[listName];
    }

    private addProductIntoProductList (productName: string){
      if (!isNullOrUndefined(productName)){
        var isPresent = false;
        if (!isNullOrUndefined(this.lists[ProductsArraysManagement.LISTA_PRODUCTOS]) && this.lists[ProductsArraysManagement.LISTA_PRODUCTOS].length > 0){
          isPresent = this.lists[ProductsArraysManagement.LISTA_PRODUCTOS].some(
            function(el: Product){ 
              return el.name === productName}
            ); 
        }
        if (!isPresent){
          this.addElementToList(ProductsArraysManagement.LISTA_PRODUCTOS, productName);
        }
      }
    }
}
