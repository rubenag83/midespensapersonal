import { Product } from './product';
import { isNullOrUndefined } from 'util';
import { TextCorrections } from './text/text-corrections';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductArrayProcessing {

    public constructor(@Inject(TextCorrections) private textCorrecctions){
        
    }

    public cleanList(list: Array<Product>): Array<Product>{
        if (!isNullOrUndefined(list)){
          list.forEach((element, index) => {
            if (isNullOrUndefined(element.name) || element.name){
              list.splice(index, 1);
            }
            else{
              element.name = this.textCorrecctions.setCapitalCase(element.name);
            }
          });
        }
        return list;
      }

    public arraySort (list: Array<Product>){
        return (!isNullOrUndefined(list)? list.sort((a, b) => a.name > b.name ? 1 : -1) : null);
    }
}
