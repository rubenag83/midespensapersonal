import { ProductArrayProcessing } from './product-array-processing';
import { TextCorrections } from './text/text-corrections';
import { Product } from './product';

describe('El procesamiento de un array de productos', () => {

  let arrayProductos = new Array<Product>();
  let productosProcesamiento = new ProductArrayProcessing(new TextCorrections());

  beforeEach(function(){
    arrayProductos = new Array<Product>();
    arrayProductos.push({name: "casa", count: 1});
    arrayProductos.push({name: "almuerzo",count: 1});
    arrayProductos.push({name: "bezolla",count: 1});
  });
  

  it('debe poder instanciarse', () => {
    expect(productosProcesamiento).toBeTruthy();
  });

  it('debe ordenar un array alfabeticamente', () =>{
    let arrayOrdenado = new Array<Product>();
    
    arrayOrdenado.push({name: "almuerzo",count: 1});
    arrayOrdenado.push({name: "bezolla",count: 1});
    arrayOrdenado.push({name: "casa",count: 1});

    expect(productosProcesamiento.arraySort(arrayProductos)).toEqual(arrayOrdenado);
  });

  it("debe revisar que no haya ningún producto sin nombre", () =>{
    arrayProductos.push({name:undefined,count:1});
    arrayProductos.push({name:"",count:1});
    arrayProductos.push({name:null,count:1});
    
    expect(productosProcesamiento.cleanList(arrayProductos).length).toEqual(3);
  });

  it("debe no hacer nada si recibe un array vacio al limpiar los nombres", () =>{
    arrayProductos = new Array<Product>();
    
    expect(productosProcesamiento.cleanList(arrayProductos).length).toEqual(0);
  });

  it("debe no hacer nada si recibe un array vacio al ordenar los productos por nombre", () =>{
    arrayProductos = new Array<Product>();
    
    expect(productosProcesamiento.arraySort(arrayProductos).length).toEqual(0);
  });
  
  it("debe no hacer nada si recibe null al limpiar los nombres", () =>{
    arrayProductos = null;

    expect(productosProcesamiento.cleanList(arrayProductos)).toEqual(null);
  });

  it("debe no hacer nada si recibe un null al ordenar los productos por nombre", () =>{
    arrayProductos = null;
    
    expect(productosProcesamiento.arraySort(arrayProductos)).toEqual(null);
  });
  
});
