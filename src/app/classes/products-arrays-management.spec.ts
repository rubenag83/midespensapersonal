import { ProductsArraysManagement } from './products-arrays-management';
import { async } from '@angular/core/testing';
import { StorageMock } from 'ionic-mocks';
import { ProductArrayProcessing } from './product-array-processing';
import { TextCorrections } from './text/text-corrections';
import { Product } from './product';

fdescribe('El gestor de arrays de productos', () => {
  let storageSpy, storageSpyEmpty, productArrayProcessing;
  let productArray : Array<Product> = [];

  var productAux = new Product();
  productAux.name = "a";
  productAux.count = 1;
  productArray.push(productAux);

  productAux = new Product();
  productAux.name = "b";
  productAux.count = 1;
  productArray.push(productAux);

  beforeEach(async(() => {

    storageSpy = StorageMock.instance(ProductsArraysManagement.LISTA_COMPRA,productArray);
    storageSpyEmpty = StorageMock.instance(ProductsArraysManagement.LISTA_EN_CASA,new Array<Product>());
    productArrayProcessing = new ProductArrayProcessing(new TextCorrections());


  }));
  it('should create an instance', () => {
    expect(new ProductsArraysManagement(storageSpy, productArrayProcessing)).toBeTruthy();
  });
  it('recupera una lista dado su nombre', () => {
    var listProduct;
    new ProductsArraysManagement(storageSpy, productArrayProcessing).getList(ProductsArraysManagement.LISTA_COMPRA).then((lista) => {
      console.log(lista);
      listProduct = lista;
      expect(listProduct).toEqual(productArray);
    });
  });

  it('devuelve una lista vacía si no existe una lista con ese nombre', () => {
    var listProduct;
    var listaVacia = new Array<Product>();
    new ProductsArraysManagement(storageSpyEmpty, productArrayProcessing).getList("aaa").then((lista) => {
      listProduct = lista;
      expect(listProduct).toEqual(listaVacia);
    });
  });

  it('puede sobreescribir un listado de productos', () => {
    let productMng = new ProductsArraysManagement(storageSpy, productArrayProcessing);
    productMng.getList("aaa");

    var productListAux = new Array<Product>();
    var productAux = new Product();
    productAux.name = "C";
    productAux.count = 1;
    productListAux.push(productAux);

    productMng.setList("aaa",productListAux);
    productMng.getList("aaa").then((lista) => {
      expect(lista).toEqual(productListAux);
    });

  });

  it('puede agregar un elemento a un listado de productos', () => {
    let productMng = new ProductsArraysManagement(storageSpy, productArrayProcessing);
    
    var productAux = new Product();
    productAux.name = "C";
    productAux.count = 3;

    productMng.getList("aaa").then((lista) => {
      

      var listaCopiada = Object.assign(Array<Product>(), lista);;

      lista = productMng.addElementToList("aaa", productAux.name.toString() ,productAux.count)

      listaCopiada.push(productAux);

      expect(lista).toEqual(listaCopiada);
    });
  });

  it('no agregara un elemento a un listado de productos si no tiene nombre', () => {
    let productMng = new ProductsArraysManagement(storageSpy, productArrayProcessing);
    
    var productAux = new Product();
    productAux.name = null;
    productAux.count = 3;

    productMng.getList("aaa").then((lista) => {
      

      var listaCopiada = Object.assign(Array<Product>(), lista);;

      lista = productMng.addElementToList("aaa", null ,productAux.count)

      console.log("Lista recuperada: " + JSON.stringify(lista));

      expect(lista).toEqual(listaCopiada);
    });
  });

  it('no agregara un elemento a un listado de productos si su cantidad no es igual o superior a 1', () => {
    let productMng = new ProductsArraysManagement(storageSpy, productArrayProcessing);
    
    var productAux = new Product();
    productAux.name = "C";
    productAux.count = -3;

    productMng.getList("aaa").then((lista) => {
      

      var listaCopiada = Object.assign(Array<Product>(), lista);;

      lista = productMng.addElementToList("aaa", productAux.name.toString() ,productAux.count)

      console.log("Lista recuperada: " + JSON.stringify(lista));

      expect(lista).toEqual(listaCopiada);
    });
  });
});
