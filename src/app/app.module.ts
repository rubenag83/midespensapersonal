import { AddProductModalPage } from './pages/add-product-modal/add-product-modal.page';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { IonicStorageModule } from '@ionic/storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [AppComponent, AddProductModalPage],
  entryComponents: [AddProductModalPage],
  imports: [
    BrowserModule, 
    FormsModule,
    IonicModule.forRoot(), 
    IonicStorageModule.forRoot({
      name: '__miDespensaPersonaldb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }), 
    AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    Clipboard,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
